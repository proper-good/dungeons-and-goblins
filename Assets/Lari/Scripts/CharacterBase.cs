using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

[RequireComponent(typeof(NavMeshAgent))]
public class CharacterBase : MonoBehaviour
{
	[SerializeField]
	protected string characterName;

	[SerializeField, Range(0f, 1000f)]
	protected float maxHP = 100f;

	[SerializeField]
	protected float movementSpeed = 3f;

	protected bool isAlive = true;
	public bool IsAlive
	{
		get { return isAlive; }
		private set
		{
			isAlive = value;
			if (!isAlive && CharacterDied != null)
				CharacterDied.Invoke();
		}
	}
	public UnityEvent CharacterDied = new UnityEvent();

	protected NavMeshAgent navMeshAgent;

	protected float currentHP;
	public virtual float CurrentHP
	{
		get
		{
			return currentHP;
		}
		protected set
		{
			if (!isAlive)
				return;

			currentHP = value;

			if (currentHP <= 0f)
			{
				Death();
			}
		}
	}

	protected virtual void Awake()
	{
		navMeshAgent = GetComponent<NavMeshAgent>();
		CurrentHP = maxHP;
	}

	public virtual void DamageCreature(float damage)
	{
		CurrentHP = Mathf.Clamp(currentHP - Mathf.Abs(damage), 0f, maxHP);
	}

	public virtual void HealCreature(float heal)
	{
		CurrentHP = Mathf.Clamp(currentHP + Mathf.Abs(heal), 0f, maxHP);
	}

	protected virtual void Death()
	{
		IsAlive = false;
	}
}
