using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour
{
	[System.Serializable]
	public class RaycastEvent : UnityEvent<RaycastHit> { }

	[System.Serializable]
	public class TargetType
	{
		[SerializeField]
		private string name;
		public string Name { get { return name; } private set { name = value; } }

		[SerializeField]
		private LayerMask layerMask;
		public LayerMask LayerMask { get { return layerMask; } private set { layerMask = value; } }

		[SerializeField]
		private RaycastEvent onSelect;
		public RaycastEvent OnSelect { get { return onSelect; } private set { onSelect = value; } }

		[SerializeField]
		private RaycastEvent onHold;
		public RaycastEvent OnHold { get { return onHold; } private set { onHold = value; } }

		// TODO: Ignored layers for holding raycast, might make moving easier
	}

	[System.Serializable]
	public class KeyAction
	{
		[SerializeField]
		private string name;
		public string Name { get { return name; } private set { name = value; } }

		[SerializeField]
		private KeyCode key;
		public KeyCode Key { get { return key; } private set { key = value; } }

		[SerializeField]
		private RaycastEvent onSelect;
		public RaycastEvent OnSelect { get { return onSelect; } private set { onSelect = value; } }

		[SerializeField]
		private RaycastEvent onHold;
		public RaycastEvent OnHold { get { return onHold; } private set { onHold = value; } }

		// TODO: Ignored layers for holding raycast, might make moving easier
	}

	[SerializeField]
	protected List<TargetType> targets = new List<TargetType>();

	[SerializeField]
	protected KeyAction[] lightActions = new KeyAction[0];

	[SerializeField]
	protected KeyAction[] darkActions = new KeyAction[0];

	protected KeyAction[] CurrentActionSet => character.DarkMode ? darkActions : lightActions;

	[SerializeField]
	private LayerMask ignoreRaycast;
	public LayerMask IgnoreRaycast { get { return ignoreRaycast; } private set { ignoreRaycast = value; } }

	protected Camera cam => Camera.main;

	protected PlayerCharacter character;

	protected virtual void Awake()
	{
		character = GetComponent<PlayerCharacter>();
	}

	protected virtual void OnEnable()
	{
		// Two part system is currently so you can move and cast abilities at the same time
		StartCoroutine(MouseInput());
		StartCoroutine(KeyInput());
	}

	protected virtual IEnumerator MouseInput()
	{
		KeyCode mouseLeft = KeyCode.Mouse0; // Hard coded of course

		while (enabled) // Do not break unless you want mouse input to stop working
		{
			if (Input.GetKeyDown(mouseLeft))
			{
				if (MouseRaycast(out RaycastHit selectionHit, ignoreRaycast))
				{
					//character.GoToPoint(hit.point);
					TargetType[] targetTypes = GetTargetTypes(selectionHit).ToArray(); // Array is more performant so that's good for holding

					// Selection holder
					if (targetTypes.Length > 0)
					{
						for (int i = 0; i < targetTypes.Length; i++)
							targetTypes[i].OnSelect?.Invoke(selectionHit); // Ray select

						while (Input.GetKey(mouseLeft))
						{
							MouseRaycast(out RaycastHit holdHit, ignoreRaycast); // Ray hold

							for (int i = 0; i < targetTypes.Length; i++)
								targetTypes[i].OnHold?.Invoke(holdHit);

							yield return null;
						}
					}
				}
			}

			yield return null;
		}

		List<TargetType> GetTargetTypes(RaycastHit hit)
		{
			List<TargetType> targetTypes = new List<TargetType>();

			// TODO?: You can optimize this by changing to for and using arrays but who cares
			foreach (TargetType targetType in targets)
				if (targetType.LayerMask == (targetType.LayerMask | (1 << hit.collider.gameObject.layer))) // If layer is included in TargetType
					targetTypes.Add(targetType);

			return targetTypes;
		}
	}

	protected virtual IEnumerator KeyInput()
	{
		while (enabled)
		{
			MouseRaycast(out RaycastHit selectionHit, ignoreRaycast);
			for (int i = 0; i < CurrentActionSet.Length; i++)
			{
				if (Input.GetKeyDown(CurrentActionSet[i].Key))
				{
					CurrentActionSet[i].OnSelect?.Invoke(selectionHit);

					if (CurrentActionSet[i].OnHold != null)
					{
						while (Input.GetKey(CurrentActionSet[i].Key))
						{
							MouseRaycast(out RaycastHit holdHit, ignoreRaycast);

							CurrentActionSet[i].OnHold.Invoke(holdHit);

							yield return null;
						}
					}
				}
			}

			yield return null;
		}
	}

	protected virtual bool MouseRaycast(out RaycastHit hit)
	{
		Ray ray = cam.ScreenPointToRay(Input.mousePosition);
		return Physics.Raycast(ray, out hit);
	}

	protected virtual bool MouseRaycast(out RaycastHit hit, LayerMask ignoreLayers)
	{
		Ray ray = cam.ScreenPointToRay(Input.mousePosition);
		return Physics.Raycast(ray, out hit, Mathf.Infinity, ~ignoreLayers);
	}
}
