using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AttackTrigger : MonoBehaviour
{
	public class ProjectileEvent : UnityEvent<Collider> { }

	[SerializeField]
	protected LayerMask targetLayers;

	[SerializeField]
	protected ProjectileEvent onHit = new ProjectileEvent();
	public ProjectileEvent OnHit { get { return onHit; } }

	protected List<Collider> colliders = new List<Collider>();

	protected virtual void OnTriggerEnter(Collider other)
	{
		if (targetLayers == (targetLayers | (1 << other.gameObject.layer))
			&& !colliders.Contains(other))
		{
			onHit?.Invoke(other);
			colliders.Add(other);
		}
	}

	protected virtual void OnTriggerExit(Collider other)
	{
		if (colliders.Contains(other))
		{
			colliders.Remove(other);
		}
	}

	public List<Collider> GetCollisions()
	{
		return colliders;
	}
}
