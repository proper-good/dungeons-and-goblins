using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
	[SerializeField]
	protected Vector3 cameraPositionNormalized = new Vector3(1f, 1f, -1f);

	[SerializeField]
	protected Vector2 cameraLimits = new Vector2(1f, 10f);

	[SerializeField, Min(0f)]
	protected float cameraZoomSpeed = 3f;

	protected float cameraDistance;

	protected PlayerCharacter playerCharacter => PlayerCharacter.PlayerInstance;

	protected virtual void Awake()
	{
		cameraDistance = cameraLimits.y;
	}

	protected virtual void Update()
	{
		UpdateCameraDistance();
		UpdateCameraPosition();
	}

	protected virtual void UpdateCameraDistance()
	{
		float scrollInput = -Input.GetAxis("Mouse ScrollWheel");
		if (scrollInput != 0f)
		{
			cameraDistance = Mathf.Clamp(cameraDistance + cameraZoomSpeed * scrollInput * Time.deltaTime, cameraLimits.x, cameraLimits.y);
		}
	}

	protected virtual void UpdateCameraPosition()
	{
		Vector3 playerPosition = playerCharacter.transform.position;
		Vector3 targetPosition = playerPosition + cameraPositionNormalized.normalized * cameraDistance;
		transform.position = targetPosition;
		transform.LookAt(playerPosition);
	}
}
