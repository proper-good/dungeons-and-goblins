using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : CharacterBase
{
	[SerializeField]
	private float dashForce = 5f;

	[SerializeField]
	protected float dashDuration = 0.3f;

	[SerializeField]
	protected float dashAnimationSpeed = 1f;

	[SerializeField]
	protected float dashCooldown = 3f;

	[SerializeField]
	protected Animator animator;
	public Animator Animator { get => animator; private set => animator = value; }

	protected bool dashing = false;

	protected bool darkMode = false;
	public bool DarkMode { get { return darkMode; } }

	protected static PlayerCharacter playerInstance;
	public static PlayerCharacter PlayerInstance { get { return playerInstance; } }

	protected override void Awake()
	{
		base.Awake();

		if (playerInstance != null && playerInstance != this)
			Destroy(gameObject);
		else
			playerInstance = this;
	}

	protected virtual void Update()
	{
		SetWalkAnimationSpeed();
	}

	public virtual void GoToPoint(Vector3 point)
	{
		navMeshAgent.speed = movementSpeed;
		navMeshAgent.SetDestination(point);
		navMeshAgent.isStopped = false;
	}

	public virtual void GoToPoint(RaycastHit hit)
	{
		GoToPoint(hit.point);
	}

	public virtual void DashTowards(Vector3 point)
	{
		if (!dashing)
			StartCoroutine(Dash(point));
	}

	public virtual void DashTowards(RaycastHit hit)
	{
		DashTowards(hit.point);
	}

	protected virtual IEnumerator Dash(Vector3 point)
	{
		dashing = true;
		animator.SetBool("Dodging", true);

		Vector3 startingVelocity = navMeshAgent.velocity;

		Vector3 dir = point - transform.position;
		dir.y = 0f;
		dir.Normalize();

		LookAt(point);
		navMeshAgent.velocity = dir * dashForce * movementSpeed;

		float timer = dashDuration;
		while (timer > 0f)
		{
			navMeshAgent.velocity = navMeshAgent.velocity.normalized * Mathf.Lerp(startingVelocity.magnitude, dashForce * movementSpeed, Mathf.Clamp01(timer / dashDuration));
			animator.SetFloat("DodgeSpeed", navMeshAgent.velocity.magnitude * dashAnimationSpeed);

			timer -= Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate();
		}

		animator.SetBool("Dodging", false);

		// Cooldown
		yield return new WaitForSeconds(dashCooldown);

		dashing = false;
	}

	public virtual void LookAt(Vector3 point)
	{
		Vector3 dir = point - transform.position;
		dir.y = 0f;
		dir.Normalize();

		if (dir.magnitude == 0f)
			return;

		transform.rotation = Quaternion.LookRotation(dir);
	}

	protected virtual void SetWalkAnimationSpeed()
	{
		animator.SetBool("Running", navMeshAgent.hasPath);
		animator.SetFloat("MovementSpeed", navMeshAgent.velocity.magnitude);
	}

	public virtual void TransformForm()
	{
		darkMode = !darkMode;
		animator.SetBool("DarkMode", darkMode);
	}

	public void IncreaseMaxHealthBy(float maxHealthIncrease)
	{
		maxHP += maxHealthIncrease;
	}
}
