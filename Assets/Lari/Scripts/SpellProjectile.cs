using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class SpellProjectile : MonoBehaviour
{
	[SerializeField]
	protected AttackTrigger trigger;

	[SerializeField]
	protected float projectileSpeed = 5f;

	[SerializeField]
	protected float projectileLife = 5f;

	protected float projectileDamage = 0f;

	protected Rigidbody rb;

	public virtual void InitializeProjectile(float damage)
	{
		Vector3 dir = transform.forward;
		rb = GetComponent<Rigidbody>();
		projectileDamage = damage;

		rb.AddForce(dir.normalized * projectileSpeed, ForceMode.VelocityChange);

		trigger.OnHit.AddListener(OnNPCHit);
		StartCoroutine(DestroyProjectile());
	}

	protected virtual IEnumerator DestroyProjectile()
	{
		yield return new WaitForSeconds(projectileLife);

		Destroy(gameObject);
	}

	protected virtual void OnCollisionEnter(Collision collision)
	{
		Destroy(gameObject);
	}

	protected virtual void OnNPCHit(Collider collider)
	{
		CharacterBase character = collider.transform.GetComponentInParent<CharacterBase>();
		if (character)
		{
			character.DamageCreature(projectileDamage);
			Destroy(gameObject);
		}
	}
}
