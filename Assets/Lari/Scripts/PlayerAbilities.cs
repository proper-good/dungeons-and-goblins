using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerCharacter))]
public class PlayerAbilities : MonoBehaviour
{
	[Header("Light mode")]
	[SerializeField]
	protected float castDelay = 0.2f;

	[SerializeField]
	protected Transform spellSpawnPoint;

	[SerializeField]
	protected GameObject fireBall;

	[SerializeField]
	protected float fireBallDamage = 50f;

	[Header("Dark mode")]
	[SerializeField]
	protected float meleeDelay = 0.5f;

	[SerializeField]
	protected AttackTrigger slash180;

	[SerializeField]
	protected AttackTrigger smash360;

	[SerializeField]
	protected float slashDamage = 50f;

	[SerializeField]
	protected float smashDamage = 35f;

	[SerializeField]
	protected GameObject debugArea;

	[Header("General")]
	[SerializeField, Min(0f)]
	protected float globalCooldownLength = 1f;

	protected bool globalCooldown = false;

	protected PlayerCharacter character;

	protected virtual void Awake()
	{
		character = GetComponent<PlayerCharacter>();
	}

	public virtual void SwordSwipe(RaycastHit hit)
	{
		if (OnCooldown())
			return;

		character.LookAt(hit.point);

		character.Animator.SetTrigger("SwordSwipe");

		MeleeDamage(slash180, slashDamage, meleeDelay);
	}

	public virtual void SwordLeapSmash(RaycastHit hit)
	{
		if (OnCooldown())
			return;

		character.DashTowards(hit);

		character.Animator.SetTrigger("SwordSmash");

		MeleeDamage(smash360, smashDamage, meleeDelay);
	}

	protected virtual bool StaffCast(RaycastHit hit)
	{
		if (OnCooldown())
			return true;

		character.LookAt(hit.point);

		character.Animator.SetTrigger("StaffCast");
		return false;
	}

	public virtual void FireBallCast(RaycastHit hit)
	{
		if (StaffCast(hit))
			return;

		StartCoroutine(FireBallCastDelay(hit));
	}

	protected virtual IEnumerator FireBallCastDelay(RaycastHit hit)
	{
		yield return new WaitForSeconds(castDelay);

		GameObject fireBallInstance = Instantiate(fireBall);
		fireBallInstance.transform.position = spellSpawnPoint.position;

		Vector3 targetPoint = hit.point;
		targetPoint.y = spellSpawnPoint.position.y;

		fireBallInstance.transform.LookAt(targetPoint);

		SpellProjectile projectile = fireBallInstance.GetComponent<SpellProjectile>();
		projectile.InitializeProjectile(fireBallDamage);
	}

	protected virtual bool OnCooldown()
	{
		if (globalCooldown)
			return true;
		else
		{
			StartCoroutine(GlobalCooldown());
			return false;
		}
	}

	protected virtual void MeleeDamage(AttackTrigger damageArea, float damage)
	{
		List<Collider> colliders = damageArea.GetCollisions();
		List<CharacterBase> characters = new List<CharacterBase>();
		foreach (Collider collider in colliders)
		{
			if (collider == null) continue;

			CharacterBase character = collider.GetComponentInParent<CharacterBase>();
			if (!characters.Contains(character))
			{
				characters.Add(character);
				character.DamageCreature(damage);
			}
		}
	}

	protected virtual void MeleeDamage(AttackTrigger damageArea, float damage, float delay)
	{
		StartCoroutine(MeleeDamageDelay(damageArea, damage, delay));
	}

	protected virtual IEnumerator MeleeDamageDelay(AttackTrigger damageArea, float damage, float delay)
	{
		yield return new WaitForSeconds(Mathf.Clamp(delay, 0f, globalCooldownLength));

		MeleeDamage(damageArea, damage);

		if (debugArea != null)
		{
			debugArea.SetActive(true);

			yield return new WaitForSeconds(0.1f);

			debugArea.SetActive(false);
		}
	}

	protected virtual IEnumerator GlobalCooldown()
	{
		globalCooldown = true;

		yield return new WaitForSeconds(globalCooldownLength);

		globalCooldown = false;
	}
}
