using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InteractableObject : MonoBehaviour
{
	[SerializeField]
	private UnityEvent _onInteract;
	public UnityEvent OnInteract { get => _onInteract; set => _onInteract = value; }
	[SerializeField]
	private float _interactDistance;
	public float InteractDistance { get => _interactDistance; set => _interactDistance = value; }

	public void Interaction(RaycastHit raycastHit, float distance)
	{
		Debug.Log($"OnInteract of InteractableObject {this.name} called.");
		if (distance <= _interactDistance)
			OnInteract.Invoke();
		else
			Debug.Log("Too far to interact");
	}
}
