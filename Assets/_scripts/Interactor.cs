using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactor : MonoBehaviour
{
	public void Interact(RaycastHit raycastHit)
	{
		InteractableObject interactableObject = raycastHit.collider.GetComponentInParent<InteractableObject>();

		if (interactableObject != null)
		{
			float distance = Vector3.Distance(raycastHit.point, transform.position);
			interactableObject.Interaction(raycastHit, distance);
		}
	}
}
