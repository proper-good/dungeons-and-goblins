using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public enum GoblinState { Idle, Moving, Attacking }

public class GoblinRogue : EnemyCharacter
{

	private PlayerCharacter player;
	public PlayerCharacter Player { get => player; set => player = value; }
	[SerializeField]
	private float engagePlayerDistance = 3.0f;
	[SerializeField]
	private float attackDistance = 2.0f;
	[SerializeField]
	private float delayBetweenAttacks;
	public float EngagePlayerDistance { get => engagePlayerDistance; set => engagePlayerDistance = value; }

	[SerializeField]
	private GoblinState currentState = GoblinState.Idle;
	public GoblinState CurrentState { get => currentState; set => currentState = value; }

	private GoblinState previousState = GoblinState.Idle;
	public GoblinState GoblinState { get => previousState; set => previousState = value; }
	public Animator Animator { get => animator; set => animator = value; }

	private Animator animator;




	protected void Start()
	{
		animator = GetComponent<Animator>();
		//navMeshAgent = GetComponent<NavMeshAgent>();
		player = FindObjectOfType<PlayerCharacter>();




		//StartCoroutine(DelayedInit(1.0f));
	}



	private void Update()
	{

		previousState = currentState;


		//Debug.Log($"agent is on navmesh = {IsAgentOnNavMesh(gameObject)}");

		if (navMeshAgent != null)
		{


			float playerDistance = Vector3.Distance(player.transform.position, transform.position);
			if (playerDistance <= engagePlayerDistance && playerDistance >= attackDistance)
			{
				currentState = GoblinState.Moving;
				//Debug.Log("Should start moving towards player..");
			}
			else if (playerDistance >= engagePlayerDistance)
			{
				currentState = GoblinState.Idle;
				//Debug.Log("Too far from player.. idling.");
			}
			else
			{
				currentState = GoblinState.Attacking;
				//Debug.Log("Should attack!");
				//attack
			}
		}

		if (previousState != currentState)
		{
			StateChanged(currentState);
		}

	}

	public void Kill()
	{
		Death();
	}
	protected override void Death()
	{
		base.Death();
		StartCoroutine(DieRoutine());

		animator.SetTrigger("");

		animator = null;
		navMeshAgent.enabled = false;
	}

	private void StateChanged(GoblinState currentState)
	{
		if (!animator)
			return;

		switch (currentState)
		{
			case GoblinState.Idle:
				//animator.SetInteger("battle", 1);
				//animator.SetInteger("moving", 0);
				animator.SetBool("Moving", false);
				navMeshAgent.isStopped = true;
				break;
			case GoblinState.Moving:
				//animator.SetInteger("moving", 2);
				navMeshAgent.isStopped = false;
				animator.SetBool("Moving", true);
				StartCoroutine(MoveTowardsPlayer());
				break;
			case GoblinState.Attacking:
				navMeshAgent.isStopped = true;
				//animator.SetInteger("moving", 0);
				animator.SetBool("Moving", false);
				StartCoroutine(Attack());
				break;
			default:
				break;
		}

	}

	public IEnumerator DieRoutine()
	{
		int random = UnityEngine.Random.Range(1, 3);

		if (random == 1)
			animator.SetTrigger("Death1");
		else
			animator.SetTrigger("Death2");

		yield return null;

		Destroy(gameObject, 10f);
	}
	public IEnumerator Attack()
	{
		if (!animator)
			yield break;

		//attack mechanics
		//animator.SetInteger("moving", 3); //atk 1 ?
		if (animator)
			animator.SetTrigger("Attack1");
		yield return new WaitForSeconds(delayBetweenAttacks);
		//animator.SetInteger("moving", 4); //atk 2 ?
		if (animator)
			animator.SetTrigger("Attack2");



	}
	public IEnumerator MoveTowardsPlayer()
	{
		while (currentState == GoblinState.Moving)
		{
			//Debug.Log("Moving towards player...");
			if (isAlive)
				navMeshAgent.SetDestination(player.transform.position);
			yield return null;
		}
	}

	public bool IsAgentOnNavMesh(GameObject agentObject)
	{
		float onMeshThreshold = 3;


		Vector3 agentPosition = agentObject.transform.position;
		NavMeshHit hit;

		// Check for nearest point on navmesh to agent, within onMeshThreshold
		if (NavMesh.SamplePosition(agentPosition, out hit, onMeshThreshold, NavMesh.AllAreas))
		{
			//Debug.Log($"agentPosition x = {agentPosition.x}, y = {agentPosition.y}, z = {agentPosition.z}");
			//Debug.Log($"sampleposition hit x = {hit.position.x}, y = {hit.position.y}, z = {hit.position.z}");
			// Check if the positions are vertically aligned
			if (Mathf.Approximately(agentPosition.x, hit.position.x)
				&& Mathf.Approximately(agentPosition.z, hit.position.z))
			{
				// Lastly, check if object is below navmesh
				return agentPosition.y >= hit.position.y;
			}
		}

		return false;
	}

	public IEnumerator DelayedInit(float delayTime)
	{
		yield return new WaitForSeconds(delayTime);



		if (!navMeshAgent.isOnNavMesh)
		{
			Debug.Log($"NAVMESHAGENT NOT BOUND {navMeshAgent.gameObject.name}");


			NavMeshHit closestHit;
			if (NavMesh.SamplePosition(transform.position, out closestHit, 100f, NavMesh.AllAreas))
			{
				transform.position = closestHit.position;
				navMeshAgent.enabled = true;
			}

			navMeshAgent.enabled = true;
		}


		navMeshAgent.enabled = true;
	}
}
