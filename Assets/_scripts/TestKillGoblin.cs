using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TestKillGoblin : MonoBehaviour
{
	public float killDistance = 1f;

	[SerializeField]
	public List<GoblinRogue> goblinRogueList;

	[SerializeField]
	public List<GoblinRogue> coblinsInVicinity = new List<GoblinRogue>();


    // Start is called before the first frame update
    void Start()
    {
		goblinRogueList = FindObjectsOfType<GoblinRogue>().ToList();
    }

    // Update is called once per frame
    void Update()
    {
		KillGoblins(); 
    }


	private void KillGoblins()
	{
		List<GoblinRogue> resultList = new List<GoblinRogue>();
		
		for (int i = 0; i < goblinRogueList.Count; i++)
		{
			if (Vector3.Distance(goblinRogueList[i].transform.position, transform.position) < killDistance)
			{
				resultList.Add(goblinRogueList[i]);
				goblinRogueList.RemoveAt(i);
			}
		}

		for (int i = 0; i < resultList.Count; i++)
		{
			resultList[i].Kill();
		}

		

	}
}
