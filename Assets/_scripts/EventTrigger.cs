using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventTrigger : MonoBehaviour
{
	[SerializeField]
	protected UnityEvent m_MyEvent = new UnityEvent();

	[SerializeField]
	protected LayerMask eventLayer;

	private void OnTriggerEnter(Collider other)
	{
		if ( eventLayer == (eventLayer | (1 << other.gameObject.layer)))

		{
			m_MyEvent?.Invoke();
		}
	}
	
}
