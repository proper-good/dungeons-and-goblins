using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
	[SerializeField]
	private GameObject enemyPrefab;

	[SerializeField]
	private List<Transform> transforms = new List<Transform>();

	[SerializeField]
	private float minTime = 5f;

	[SerializeField]
	private float maxTime = 10f;

	private float timer = 0f;
	private void Update()
	{
		if (timer <= 0f)
		{
			Instantiate(enemyPrefab);
			int positionIndex = Random.Range(0, transforms.Count);
			enemyPrefab.transform.position = transforms[positionIndex].position;

			timer = Random.Range(minTime, maxTime);
		}
		else
			timer -= Time.deltaTime;
	}
}
