using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableDoor : MonoBehaviour
{
	private Animator _animator;

	private void Start()
	{
		_animator = gameObject.GetComponentInChildren<Animator>();
	}

	public void Open()
	{
		_animator.SetBool("IsOpen", true);
	}
	public void Close()
	{
		_animator.SetBool("IsOpen", false);
	}

	public void Toggle()
	{
		_animator.SetBool("IsOpen", !_animator.GetBool("IsOpen"));
	}



}
