using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DropableObject
{
	//This will never be duplicated
	[SerializeField]
	protected bool objectIsAlwaysRequired = true;
	public bool ObjectIsAlwaysRequired { get { return objectIsAlwaysRequired; } }

	//Cand force non-/duplication for other items here
	[SerializeField]
	protected bool objectCanBeDuplicated = true;
	public bool ObjectCanBeDuplicated { get { return objectCanBeDuplicated; } }

	[SerializeField]
	protected GameObject objectGameObject;
	public GameObject ObjectGameObject { get { return objectGameObject; } }

}

public class ObjectDropper : MonoBehaviour
{
	[SerializeField]
	private CharacterBase character;

	[SerializeField]
	private float maxSpawnRange = 2f;

	[SerializeField]
	private float maxHeightOfArc = 2f;

	[SerializeField]
	protected int numOfObjectToDropIfRandomisedMin = 2;

	[SerializeField]
	protected int numOfObjectToDropIfRandomisedMax = 5;

	protected int numOfObjectToDropIfRandomised = 0;

	[SerializeField]
	protected bool randomiseObjectDropping;

	[SerializeField]
	protected List<DropableObject> objectsToDrop;

	private int randomIndex = 0;

	private void Awake()
	{
		character.CharacterDied.AddListener(DropObjects);
	}

	protected void OnEnable()
	{
		if (randomiseObjectDropping)
		{
			RandomiseObjects();
		}
	}

	protected void RandomiseObjects()
	{
		numOfObjectToDropIfRandomised = UnityEngine.Random.Range(numOfObjectToDropIfRandomisedMin, numOfObjectToDropIfRandomisedMax);

		List<DropableObject> tempList = new List<DropableObject>();

		//Add the required items to the list first and removed from original list
		for (int i = objectsToDrop.Count - 1; i > -1; i--)
		{
			if (objectsToDrop[i].ObjectIsAlwaysRequired)
			{
				numOfObjectToDropIfRandomised--;
				tempList.Add(objectsToDrop[i]);
				objectsToDrop.RemoveAt(i);
			}
		}

		//Start getting random items
		for (int i = 0; i < numOfObjectToDropIfRandomised; i++)
		{
			//If the list to randomise from is empty then break out of the loop
			if (objectsToDrop.Count == 0)
			{
				break;
			}

			randomIndex = UnityEngine.Random.Range(0, objectsToDrop.Count - 1);
			tempList.Add(objectsToDrop[randomIndex]);
			if (objectsToDrop[randomIndex].ObjectCanBeDuplicated == false)
			{
				objectsToDrop.RemoveAt(randomIndex);
			}
		}

		objectsToDrop = tempList;
	}

	protected void DropObjects()
	{
		for (int i = 0; i < objectsToDrop.Count; i++)
		{
			GameObject spawnedObject = Instantiate(objectsToDrop[i].ObjectGameObject, GetRandomisedPosition(), Quaternion.identity);

			float spawnRotation = UnityEngine.Random.Range(0f, 360f);
			spawnedObject.transform.rotation = Quaternion.Euler(0f, spawnRotation, 0f);
		}
	}

	private Vector3 GetRandomisedPosition()
	{
		Vector3 dir = Vector3.forward;
		float randomRotation = UnityEngine.Random.Range(0f, 360f);
		float randomDistance = UnityEngine.Random.Range(0f, maxSpawnRange);
		dir = Quaternion.AngleAxis(randomRotation, Vector3.up) * dir;
		Vector3 spawnPos = transform.position + dir * randomDistance;

		RaycastHit hit;
		if (Physics.Raycast(spawnPos + Vector3.up * 0.1f, Vector3.down, out hit) && hit.transform.gameObject.layer.CompareTo(LayerMask.NameToLayer("Ground")) == 0)
			spawnPos = hit.point;
		else
			spawnPos = transform.position;

		return spawnPos;
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.DownArrow))
		{
			DropObjects();
		}
	}
}
