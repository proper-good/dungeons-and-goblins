namespace Crummy
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	public class CreatureBase : MonoBehaviour
	{
		[SerializeField]
		protected string creatureName;

		[SerializeField, Range(0f, 1000f)]
		protected float maxHP;

		[SerializeField]
		protected float movementSpeed = 100f;

		protected float currentHP;

		protected bool isAlive;
		public virtual float CurrentHP
		{
			get
			{
				return currentHP;
			}
			protected set
			{
				if (!isAlive)
				{
					return;
				}

				currentHP = value;
				if (currentHP <= 0)
				{
					Death();
				}
			}
		}


		protected virtual void Awake()
		{
			CurrentHP = maxHP;
		}

		public virtual void DamageCreature(float dmg)
		{
			CurrentHP = Mathf.Clamp(currentHP - Mathf.Abs(dmg), 0f, maxHP);
		}
		public virtual void HealCreature(float heal)
		{
			CurrentHP = Mathf.Clamp(currentHP + Mathf.Abs(heal), 0f, maxHP);
		}

		protected virtual void Death()
		{
			this.isAlive = false;
		}
	}
}
